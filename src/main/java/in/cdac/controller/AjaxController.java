package in.cdac.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.cdac.dao.UserDao;
import in.cdac.model.Users;
@RestController
public class AjaxController {
	
	  @Autowired
	  UserDao userDao;
	
	@RequestMapping(value="/ajax/getUserList", method=RequestMethod.GET)
	public List<Users> getUserList(){
		return userDao.getUserList();
	}

}
