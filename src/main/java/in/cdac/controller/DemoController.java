package in.cdac.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import in.cdac.model.Users;
import in.cdac.service.UserService;

@Controller
public class DemoController {

	
	
	@Autowired
	UserService userService;
	static Logger log = Logger.getLogger(DemoController.class.getName());


	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String adminPage(Model model, Principal principal) {
         List<Users> UserList=userService.getUserList();
         model.addAttribute("UserList",UserList);
		return "adminPage";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(Model model) {
	
	
			    log.debug("Hello this is a debug message");
			    log.info("Hello this is an info message");
		return "loginPage";
	}

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String registrationPagee(Model model) {
		model.addAttribute("user", new Users());
		
		return "registration";
	}

	@RequestMapping(value = { "/registration" }, method = RequestMethod.POST)
	public String createUser(@Valid @ModelAttribute("user") Users user, BindingResult bindingResult, Model model) {
		
		  String pass1=user.getEncrytedpass(); 
		  String pass2=user.getEncryptedpass1();
		  System.out.println(pass1+""+pass2);
		
		  if(!user.getEncrytedpass().equals(user.getEncryptedpass1())) {
		  bindingResult.rejectValue("encryptedpass1", "encryptedpass1.valid");
		  
		  }
		 
		Boolean existing = userService.findByEmail(user.getEmailid());
		System.out.println(existing);
		if (existing != false) {

			bindingResult.rejectValue("emailid", "emailid.valid");
		}

		
		  if (bindingResult.hasErrors()) { return "registration"; }
		 
		System.out.println(user);
		try {
			
			userService.addUsers(user);
			
			model.addAttribute("success_message", user.getName()
					+ " registered sucessfully.");
				
			System.out.println("inserted......");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			model.addAttribute("erroeMessage",
					user.getName() + " not registered");
		}

		return "registration";
	}

	@RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
	public String logoutSuccessfulPage(HttpServletRequest request, HttpServletResponse response) {
		
		 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
         if (auth != null) {
             new SecurityContextLogoutHandler().logout(request, response, auth);
         }
		return "loginPage";
	}

	@RequestMapping(value = "/user", method = RequestMethod.GET)
	public String userInfo(Model model, Principal principal) {
       
		String userName = principal.getName();
	

		model.addAttribute("userInfo", userName);

		return "userInfoPage";
	}

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accessDenied(Model model, Principal principal) {

		String message = "Hi " + principal.getName() //
				+ "<br> You do not have permission to access this page!";
		model.addAttribute("message", message);

		return "403Page";
	}
	

}
