package in.cdac;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


import in.cdac.model.Users;

public class Mapper implements RowMapper<Users> {

	@Override
	public Users mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		/*
		 * System.out.println("#############################\n"); ResultSetMetaData rsm
		 * = rs.getMetaData(); for(int i=0; i<rsm.getColumnCount();i++) {
		 * System.out.println(rsm.getColumnName(i+1)+"->"+rs.getObject(i+1)); }
		 */

        Users user=new Users();
        user.setEmailid(rs.getString("emailid"));
        user.setName(rs.getString("name"));
        user.setMobileno(rs.getString("mobileno"));
        user.setCity(rs.getString("city"));
        user.setEncrytedpass(rs.getString("encryptedpass"));
        
		return user;
	}
	
	
	

}
