package in.cdac.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import in.cdac.Mapper;

import in.cdac.model.Users;
import in.cdac.service.UserDetailsServiceImpl;


@Repository
public class UserDao {
	
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	

	 public List<String> getRoleNames(String emailid) {

	       String sql="select r.rolename from user_role ur,roles r where ur.roleid=r.roleid and ur.emailid=?";
	        Object[] params = new Object[] { emailid };
	 
	        List<String> roles = jdbcTemplate.queryForList(sql, params, String.class);
	 
	        return roles;
	    }


	    public Users findUserAccount(String emailid) {
	     
	        String sql =  "select * from users where emailid = ? ";
	 
	       
	        Mapper mapper = new Mapper();
	        try {
	            Users userInfo = jdbcTemplate.queryForObject(sql, new Object[] { emailid }, mapper);
	            return userInfo;
	        } catch (EmptyResultDataAccessException e) {
	            return null;
	        }
	    }
	    
	    
		
		 public List<Users> getUserList() {
	
			     String sql="select * from users";
		        
		        List<Users> users = jdbcTemplate.query(sql,new Mapper());
		 
		        return users;
		    }
		 
			public boolean addUsers(Users user) throws Exception{
				String encrytedPassword = UserDetailsServiceImpl.encrytePassword(user.getEncrytedpass());
				String sql ="insert into users values(?,?,?,?,?)"; 
				String sql2="select roleid from roles where rolename=?";
				jdbcTemplate.update(sql,new Object[] {user.getEmailid(),user.getName(),user.getMobileno(),user.getCity(),encrytedPassword});
				String userrole="ROLE_USER";
				int roleid=jdbcTemplate.queryForObject(sql2,new Object[]{userrole},Integer.class);
				System.out.println("RoleId===="+roleid);
				///insert into user_role values(?,?)
				String sql1="insert into user_role values(?,?)";
				jdbcTemplate.update(sql1,new Object[] {user.getEmailid(),roleid});
				
				
				return true;
			}

	

			public Boolean findByEmail(String emailid) {
				// TODO Auto-generated method stub
				String sql="SELECT EXISTS(SELECT * FROM users WHERE emailid=?)";
				Boolean exist =jdbcTemplate.queryForObject(sql,new Object[]{emailid},boolean.class);
				if(exist==true)
				return true;
				else
					return false;
			}
	    

	    
}
