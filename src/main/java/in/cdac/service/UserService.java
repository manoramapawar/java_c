package in.cdac.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.cdac.dao.UserDao;
import in.cdac.model.Users;
@Service
public class UserService {
	
	@Autowired
	UserDao userDao;

	public Boolean findByEmail(String emailid) {
		// TODO Auto-generated method stub
		return userDao.findByEmail(emailid);
	}

	public void addUsers(@Valid Users user) throws Exception {
		// TODO Auto-generated method stub
		userDao.addUsers(user);
		
	}
	
	public List<Users> getUserList(){
		return  userDao.getUserList();
	}
	
	
	
	
	

}
