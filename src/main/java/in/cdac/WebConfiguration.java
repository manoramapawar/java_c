package in.cdac;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {
	
	  
	  @Override 
	  public void addViewControllers(ViewControllerRegistry registry) {
	      registry.addRedirectViewController("/", "/login");
	   }
	  @Bean
	  public MessageSource messageSource() {
	      ReloadableResourceBundleMessageSource messageSource
	        = new ReloadableResourceBundleMessageSource();
	      
	      messageSource.setBasename("classpath:messages");
	      messageSource.setDefaultEncoding("UTF-8");
	      return messageSource;
	  }
}