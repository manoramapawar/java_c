package in.cdac.config;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggerConfig {
	
	private static final Logger LOGGER=Logger.getLogger(LoggerConfig.class);

	@Around("execution(* in.cdac.controller..*(..))")
	public Object log(ProceedingJoinPoint joinPoint) throws Throwable {
		
		LOGGER.info("calling method: "+joinPoint.getSignature().toLongString());
		
		  Object []args = joinPoint.getArgs(); for(Object arg : args) {
		  LOGGER.info(arg.toString()); }
		 
		Object result = joinPoint.proceed();
		
		LOGGER.info("End of method with result as: "+joinPoint.getSignature().toLongString()+" with result: "+result);
		
		return result;
	}
	
}
