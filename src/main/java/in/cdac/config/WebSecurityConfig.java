package in.cdac.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import in.cdac.service.CustomSuccessHandler;
import in.cdac.service.UserDetailsServiceImpl;
 
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
 
    @Autowired
    UserDetailsServiceImpl userDetailsService;
    @Autowired
    CustomSuccessHandler customhandler;
 
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }
     
     
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception { 
 
        // Setting Service to find User in the database.
        // And Setting PassswordEncoder
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());     
 
    }
 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
 
        http.csrf().disable()
 
        // The pages does not require login
       .authorizeRequests().antMatchers( "/login1","/login", "/logout","/createUserBySuperAdmin").permitAll()
 
        // /userInfo page requires login as ROLE_USER or ROLE_ADMIN.
        // If no login, it will redirect to /login page.
       .antMatchers("/user").access("hasRole('ROLE_USER')")
 
        // For ADMIN only.
        .antMatchers("/admin").access("hasRole('ROLE_ADMIN')")
 
        // When the user has logged in as XX.
        // But access a page that requires role YY,
        // AccessDeniedException will be thrown.
        .and().exceptionHandling().accessDeniedPage("/403")
 
        // Config for Login Form
        .and().formLogin()//
                // Submit URL of login page.
                .loginProcessingUrl("/j_spring_security_check") // Submit URL
                .loginPage("/login")//
                .successHandler(customhandler)
              // .defaultSuccessUrl("/user",true)//
                .failureUrl("/login?error=true")//
               .usernameParameter("username")//
               .passwordParameter("password")
                // Config for Logout Page
                .and().logout().
             logoutUrl("/logoutSucessful").logoutSuccessUrl("/logoutSuccessful").clearAuthentication(true).invalidateHttpSession(true);
 
    }
}