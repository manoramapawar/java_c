package in.cdac.model;



public class Users {
	
    
    private String emailid;
	
	private String name;

    private String mobileno;
    private String city;

    private String encrytedpass;

    private String encryptedpass1;
   

	public Users() {
	
		// TODO Auto-generated constructor stub
	}


	public Users(String emailid, String name, String mobileno, String city, String encrytedpass,
			String encryptedpass1) {
		super();
		this.emailid = emailid;
		this.name = name;
		this.mobileno = mobileno;
		this.city = city;
		this.encrytedpass = encrytedpass;
		this.encryptedpass1 = encryptedpass1;
	}


	public String getEmailid() {
		return emailid;
	}


	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getMobileno() {
		return mobileno;
	}


	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getEncrytedpass() {
		return encrytedpass;
	}


	public void setEncrytedpass(String encrytedpass) {
		this.encrytedpass = encrytedpass;
	}

	 
    public String getEncryptedpass1() {
		return encryptedpass1;
	}


	public void setEncryptedpass1(String encryptedpass1) {
		this.encryptedpass1 = encryptedpass1;
	}


	@Override
	public String toString() {
		return "Users [emailid=" + emailid + ", name=" + name + ", mobileno=" + mobileno + ", city=" + city
				+ ", encrytedpass=" + encrytedpass + ", encryptedpass1=" + encryptedpass1 + "]";
	}


	
}
