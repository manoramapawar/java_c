<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<title>Registration</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
<title>Insert title here</title>
<style type="text/css">
	

	.error {
		color: red;
	/* 	font-style: italic; */
	}


</style>
</head>

<c:url var="val" value="/registration" />
<body>

<div class="container">
     <div class="row" style="height:10px">
	
          </div>
        
        <div class="row" style="height:25px">
        <div class="col-5"></div>
			<div class="col-4">
			
        <h4>Registration Form</h4></div>
        
       <div class="col-3"><a href="login">Home</a></div>
        </div>
        <div class="row" style="height:25px">
        
        </div>
		<div class="row" >
			<div class="col-3"></div>
			<div class="col-6 ">
                  <c:if test="${success_message ne null}">
			<div class="alert alert-success">
				 ${success_message}
			</div>

		</c:if>
		
		
		<c:if test="${erroeMessage ne null}">
			<div class="alert alert-danger">
			 ${erroeMessage}
			</div>

		</c:if>
      
	
				<form:form modelAttribute="user"  method="post" id="regForm"  action="${val}" >
	                 <div class="form-group">       
				  <label>User Name</label>
				    <input type="email" name="emailid" class="form-control" id="emailid" placeholder="Enter emailId as username" required data-error="Email is required">
				     <form:errors path="emailid" cssClass="error" />
				  </div>        
				   <div class="form-group">       
				    <label>Name</label>
				    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" required data-error="Name is required">
				  </div>
				  <div class="form-group">       
				    <label>Mobile Number</label>
				    <input type="text" name="mobileno" class="form-control" id="mobileno" placeholder="Enter mobile number" required data-error="Mobile number is required">
				  </div>
				   <div class="form-group">       
				    <label>City</label>
				    <input type="text" name="city" class="form-control" id="city" placeholder="Enter city" required data-error="City is required">
				  </div>
				  <div class="form-group">
				    <label >Password</label>
				    <input type="password" name="encrytedpass" class="form-control" id="pass" placeholder="Enter password" required data-error="Password is required">
                   
                  </div>
                  	  <div class="form-group">
				    <label>Confirm Password</label>
				    <input type="password" name="encryptedpass1" class="form-control" id="pass1" placeholder="Enter confirm password" required data-error="Password is required">
                     <form:errors path="encryptedpass1" cssClass="error" />
                  </div>
   

				  <button type="submit" class="btn btn-primary"  >Submit</button>
				</form:form>
		</div>
		<div class="col-3"></div>
	</div>
	</div>
	

</body>

</html>