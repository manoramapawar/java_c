<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
</head>
<c:url var="val" value="/j_spring_security_check" />
<body>
	<div class="container">
        <div class="row" style="height:100px"></div>

		<div class="row" >
			<div class="col-3"></div>
			<div class="col-6 ">
            <c:if test="${param.error ne null}">
			<div style="color: red">Invalid credentials.</div>
		      </c:if>
				<form method="post" action="${val}">
                       
				   <div class="form-group">       
				    <label>UserName</label>
				    <input type="email" name="username" class="form-control" required>
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Password</label>
				    <input type="password" name="password" class="form-control" required>
                  </div>

				  <button type="submit" class="btn btn-primary">Submit</button>
				  <div style="height: 40px;"></div>
				  <div>
				  	New User?<a href="registration">Register</a></div>
				</form>
		</div>
		<div class="col-3"></div>
	</div>
	</div>
</body>
</html>