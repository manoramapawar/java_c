<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>

<html>

<head>
<meta charset="ISO-8859-1">
<title>Admin Page</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<!-- <link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css"> -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
 -->
 <link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/jquery.dataTables.min.css">
	 <link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/buttons.dataTables.min.css">
	 <link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/select.dataTables.min.css">
	
		<script src="${pageContext.request.contextPath}/js/jquery-3.5.1.js"></script>
		<script src="${pageContext.request.contextPath}/js/jquery.dataTables.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/dataTables.buttons.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/buttons.flash.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/jszip.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/pdfmake.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/vfs_fonts.js"></script>
		<script src="${pageContext.request.contextPath}/js/buttons.html5.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/buttons.print.min.js"></script>
		<script src="${pageContext.request.contextPath}/js/dataTables.select.min.js"></script>		
		








</head>
<script type="text/javascript">

$(document).ready( function () {
	 var table = $('#employeesTable').DataTable({
	        dom: 'lBfrtip',
	        buttons: [
	            'copy',
	            'csv',
	            'excel',
	            'pdf',
	            {
	                extend: 'print',
	                text: 'Print',
	                exportOptions: {
	                    modifier: {
	                        selected: null
	                    }
	                }
	            }
	        ],
	        
	        select: true,

			 "scrollY": 200,
			 "rowCallback": function (nRow, aData, iDisplayIndex) {
			     var oSettings = this.fnSettings ();
			     $("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
			     return nRow;
			}

	 })
	 
	

});

</script>
<body>
	<div class="container">
	<!-- 	<div class="row" style="height: 100px"></div> -->
		<div class="row" style="height: 100px; align-content: center; justify-content: center;"><h1>User List</h1>
		</div>
			<div class="row" style="height: 50px">
			<div class="col-9"></div>
			<div class="col-3"><a href="logoutSuccessful">logout</a></div>
		</div>
	
		<div class="row" style="height: 50px"></div>
		<!-- 	<div class="col-3"></div>
			<div class="col-6 "> -->

<div class="row" >
   <div class="col-1"></div>
    <div class="col-10">
	<table id="employeesTable" class="display">
      
       <!-- Header Table -->
       <thead>
           <tr ><th colspan="6"></th></tr>
				<tr>
				  <th>SrNo</th>
                    <th>EmailId</th>
				<th>Name</th>
                <th>Mobile Number</th>
                <th>City</th>
                
              </tr>   
              </thead>
             
                <tbody>
                <c:forEach var="listuser" items="${UserList}" varStatus="status">
                <tr>
                    <td></td>
                    <td>${listuser.emailid}</td>
                    <td>${listuser.name}</td>
                    <td>${listuser.mobileno}</td>
                    <td>${listuser.city}</td>
                    
                 
    
                             
                </tr>
                </c:forEach> </tbody> 
    
    </table>
    
    </div>
       <div class="col-1"></div>
    </div>

	
	</div>

</body>
</html>
