FROM openjdk:8-jdk-alpine
COPY ./target/demo-project-0.0.1-SNAPSHOT.jar demo-project-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/demo-project-0.0.1-SNAPSHOT.jar"]

